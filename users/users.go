/*

auth-homecare

Copyright (c) 2020, Fco. Javier García Vázquez <xavie2a@gmail.com>

auth-homecare is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
Public License version 3. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
You should have received a copy of the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.

*/

package users

import (
	"auth-homecare/contracts"
	"auth-homecare/db"
	"errors"

	"github.com/jbrodriguez/mlog"
	uuid "github.com/satori/go.uuid"
	"golang.org/x/crypto/bcrypt"
)

// Errors returned
var (
	ErrEmptyBody       = errors.New("Empty body")
	ErrMalformed       = errors.New("Malformed JSON")
	ErrEmptyUser       = errors.New("Empty field (username or password)")
	ErrUserNotFound    = errors.New("User not found")
	ErrInvalidPassword = errors.New("Invalid password")
	ErrCreateJWT       = errors.New("Failed to create JWT")
)

var dbClient *db.BoltClient

// Init initializes authentication service
func Init() error {
	var err error
	dbClient, err = db.OpenDatabase("auth.db")
	return err
}

// SetPassword sets user password
func SetPassword(username, password, role string) error {
	data, err := GetUserByNameAndRole(username, role)
	if err != nil {
		return ErrUserNotFound
	}

	p, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return errors.New("Failed to generate new password: " + err.Error())
	}
	data.Password = string(p)

	if err := dbClient.Update(data, data.ID); err != nil {
		return errors.New("Failed to update admin password: " + err.Error())
	}

	return nil
}

// CreateUser creates new user account based on provided username and password.
// The account is assigned with one master key - a key with all permissions on
// all owned resources regardless of their type. Provided password in encrypted
// using bcrypt algorithm.
func CreateUser(username, password, role string) (string, error) {
	var user contracts.User

	if username == "" || password == "" {
		return "", errors.New("Empty field (username or password)")
	}

	if role != "patient" && role != "doctor" {
		return "", errors.New("Invalid Role")
	}

	err := dbClient.GetByNameAndRole(&user, username, role)
	if err == nil {
		return "", errors.New("Username already exists")
	}

	p, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return "", errors.New("Error encrypting user password: " + err.Error())
	}

	user.ID = uuid.NewV4().String()
	user.Username = username
	user.Password = string(p)
	user.Role = role

	if err := dbClient.Add(user, user.ID); err != nil {
		return "", errors.New("Failed to insert user: " + err.Error())
	}

	return user.ID, nil
}

// CreateDefaultUser creates default user, if no user is defined
func CreateDefaultUser(username, password, role string) {
	users := []contracts.User{}

	err := dbClient.GetUsersByRole(&users, role)
	if err != nil {
		mlog.Error(errors.New("Couldn't get users from DB: " + err.Error()))
		return
	}

	if len(users) == 0 {
		_, err = CreateUser(username, password, role)
		if err != nil {
			mlog.Error(errors.New("Error creating default doctor: " + err.Error()))
			return
		}

		mlog.Info("Default doctor %s registered", username)
	}
}

// CheckPasswordAndRole checks user password
func CheckPasswordAndRole(username, password, role string) error {
	if username == "" || password == "" || role == "" {
		return ErrEmptyUser
	}
	user := contracts.User{}
	err := dbClient.GetByNameAndRole(&user, username, role)
	if err != nil {
		return ErrUserNotFound
	}

	err = checkPassword(password, user.Password)
	if err != nil {
		return ErrInvalidPassword
	}

	return nil
}

// ChangePassword changes user password
func ChangePassword(username, password string) error {
	role := "role"
	data, err := GetUserByNameAndRole(username, role)
	if err != nil {
		return ErrUserNotFound
	}

	p, _ := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	data.Password = string(p)

	if err := dbClient.Update(data, data.ID); err != nil {
		return errors.New("Couldn't update password")
	}

	return nil
}

// GetUserByID gets user by ID
func GetUserByID(id string) (contracts.User, error) {
	user := contracts.User{}
	err := dbClient.GetByID(&user, id)
	return user, err
}

// GetUserByNameAndRole gets user by ID
func GetUserByNameAndRole(username, role string) (contracts.User, error) {
	user := contracts.User{}
	err := dbClient.GetByNameAndRole(&user, username, role)
	return user, err
}

// DeleteUser deletes user using ID
func DeleteUser(id string) error {
	_, err := GetUserByID(id)
	if err != nil {
		return err
	}

	return dbClient.DeleteByID(id)
}

// CheckPassword tries to determine whether or not the submitted password
// matches the one stored (and hashed) during registration. An error will be
// used to indicate an invalid password.
func checkPassword(plain, hashed string) error {
	return bcrypt.CompareHashAndPassword([]byte(hashed), []byte(plain))
}
