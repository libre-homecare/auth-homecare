/*

auth-homecare

Copyright (c) 2020, Fco. Javier García Vázquez <xavie2a@gmail.com>

auth-homecare is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
Public License version 3. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
You should have received a copy of the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.

*/

package rest

import (
	"net/http"
	"strconv"

	"github.com/go-zoo/bone"
	"github.com/jbrodriguez/mlog"
)

const (
	ping = "/ping"
)

// HTTPServer function
func httpServer() http.Handler {
	mux := bone.New()

	mux.Get(ping, http.HandlerFunc(replyPing))

	mux.Post("/users", http.HandlerFunc(createUser))
	mux.Delete("/users/:id", http.HandlerFunc(deleteUser))
	mux.Put("/changePass", http.HandlerFunc(changePassword))

	mux.Post("/login", http.HandlerFunc(login))
	mux.Get("/authPatient", http.HandlerFunc(authorizePatient))
	mux.Get("/authDoctor", http.HandlerFunc(authorizeDoctor))

	return mux
}

// StartHTTPServer starts HTTP server
func StartHTTPServer(errChan chan error, port int) {
	go func() {
		mlog.Info("Listening on port: " + strconv.Itoa(port))
		errChan <- http.ListenAndServe(":"+strconv.Itoa(port), httpServer())
	}()
}
