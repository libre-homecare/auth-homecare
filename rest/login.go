/*

auth-homecare

Copyright (c) 2020, Fco. Javier García Vázquez <xavie2a@gmail.com>

auth-homecare is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
Public License version 3. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
You should have received a copy of the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.

*/

package rest

import (
	"auth-homecare/auth"
	"auth-homecare/contracts"
	"auth-homecare/users"
	"encoding/json"
	"errors"
	"io"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/jbrodriguez/mlog"
)

// Errors returned
var (
	ErrEmptyBody     = errors.New("Empty body")
	ErrMalformed     = errors.New("Malformed JSON")
	ErrCreateJWT     = errors.New("Failed to create JWT")
	ErrEmptyUser     = errors.New("Empty field (username or password)")
	ErrUserNotFound  = errors.New("User not found")
	ErrLoginAttempts = errors.New("Too many login attempts, wait 60 sec")
)
var lastAttempt time.Time
var numAttempt int

func login(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		mlog.Error(ErrEmptyBody)
		http.Error(w, ErrEmptyBody.Error(), http.StatusBadRequest)
		return
	}

	var user contracts.User
	if err = json.Unmarshal(body, &user); err != nil {
		mlog.Error(ErrMalformed)
		http.Error(w, ErrMalformed.Error(), http.StatusBadRequest)
		return
	}

	err = checkUser(user)
	if err != nil {
		switch err {
		case ErrEmptyUser:
			http.Error(w, err.Error(), http.StatusBadRequest)
		case ErrLoginAttempts:
			http.Error(w, err.Error(), http.StatusServiceUnavailable)
		case ErrUserNotFound:
			http.Error(w, err.Error(), http.StatusNotFound)
		default:
			http.Error(w, err.Error(), http.StatusUnauthorized)
		}
		mlog.Error(err)
		return
	}

	jwt, err := auth.CreateKey(user.Username, user.Role)
	if err != nil {
		mlog.Error(ErrCreateJWT)
		http.Error(w, ErrCreateJWT.Error(), http.StatusInternalServerError)
		return
	}

	mlog.Info("User " + user.Username + " logged successfully")
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	_, _ = io.WriteString(w, `{"token":"`+jwt+`"}`)
}

func checkUser(user contracts.User) error {
	// Don't allow more than 2 failed attempts per minute
	if numAttempt > 2 {
		diff := time.Since(lastAttempt)
		if diff < (time.Second * 60) {
			return ErrLoginAttempts
		}
		numAttempt = 0
	}
	lastAttempt = time.Now()

	// Check user & password
	err := users.CheckPasswordAndRole(user.Username, user.Password, user.Role)
	if err != nil {
		numAttempt++
		return err
	}

	numAttempt = 0

	return nil
}
