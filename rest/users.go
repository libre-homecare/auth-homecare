/*

auth-homecare

Copyright (c) 2020, Fco. Javier García Vázquez <xavie2a@gmail.com>

auth-homecare is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
Public License version 3. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
You should have received a copy of the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.

*/

package rest

import (
	"auth-homecare/contracts"
	"auth-homecare/users"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"

	"github.com/go-zoo/bone"
	"github.com/jbrodriguez/mlog"
)

// Errors returned
var (
	ErrCreateUser  = errors.New("Couldn't create user")
	ErrGetUsers    = errors.New("Couldn't get all users")
	ErrGetUser     = errors.New("Couldn't get user")
	ErrDeleteUser  = errors.New("Couldn't delete user")
	ErrEmptyFields = errors.New("Some fields are empty")
	ErrUserExist   = errors.New("User already exist")
	ErrUserRole    = errors.New("Invalid user's Role")
)

// Handles change user password request
func changePassword(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		mlog.Error(ErrEmptyBody)
		http.Error(w, ErrEmptyBody.Error(), http.StatusBadRequest)
		return
	}

	changePass := &contracts.ChangePass{}
	if err = json.Unmarshal(body, changePass); err != nil {
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if changePass.Username == "" || changePass.Password == "" || changePass.NewPassword == "" {
		mlog.Error(ErrEmptyFields)
		http.Error(w, ErrEmptyFields.Error(), http.StatusBadRequest)
		return
	}

	mlog.Info("username: " + changePass.Username + " Role: " + changePass.Role)
	_, err = users.GetUserByNameAndRole(changePass.Username, changePass.Role)
	if err != nil {
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	err = users.CheckPasswordAndRole(changePass.Username, changePass.Password, changePass.Role)
	if err != nil {
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusUnauthorized)
		return
	}

	err = users.SetPassword(changePass.Username, changePass.NewPassword, changePass.Role)
	if err != nil {
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	mlog.Info("User %s password changed", changePass.Username)
}

// Handles create a new user request
func createUser(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		mlog.Error(ErrEmptyBody)
		http.Error(w, ErrEmptyBody.Error(), http.StatusBadRequest)
		return
	}

	data := &contracts.User{}
	if err = json.Unmarshal(body, data); err != nil {
		mlog.Error(ErrCreateUser)
		http.Error(w, ErrCreateUser.Error(), http.StatusBadRequest)
		return
	}

	if data.Username == "" || data.Password == "" {
		mlog.Error(ErrEmptyFields)
		http.Error(w, ErrEmptyFields.Error(), http.StatusBadRequest)
		return
	}

	if data.Role != "doctor" && data.Role != "patient" {
		mlog.Error(ErrUserRole)
		http.Error(w, ErrEmptyFields.Error(), http.StatusBadRequest)
		return
	}

	_, err = users.GetUserByNameAndRole(data.Username, data.Role)
	if err == nil {
		mlog.Error(ErrUserExist)
		http.Error(w, ErrUserExist.Error(), http.StatusBadRequest)
		return
	}

	data.ID, err = users.CreateUser(data.Username, data.Password, data.Role)
	if err != nil {
		mlog.Error(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	mlog.Info("New user %s registered with ID %s", data.Username, data.ID)
	w.WriteHeader(http.StatusCreated)
	_, _ = w.Write([]byte(data.ID))
}

// Delete user using ID
func deleteUser(w http.ResponseWriter, r *http.Request) {
	id := bone.GetValue(r, "id")

	err := users.DeleteUser(id)
	if err != nil {
		mlog.Error(ErrDeleteUser)
		http.Error(w, ErrDeleteUser.Error(), http.StatusNotFound)
		return
	}

	mlog.Info("User %s deleted", id)
}
