/*

auth-homecare

Copyright (c) 2020, Fco. Javier García Vázquez <xavie2a@gmail.com>

auth-homecare is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
Public License version 3. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
You should have received a copy of the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.

*/

package rest

import (
	"auth-homecare/auth"
	"errors"
	"net/http"

	"github.com/jbrodriguez/mlog"
)

// Errors returned
var (
	ErrMissing = errors.New("Missing Authorization header")
	ErrInvalid = errors.New("Invalid token")
)

func replyPing(w http.ResponseWriter, _ *http.Request) {
	w.Header().Set("Content-Type", "text/plain; charset=utf-8")
	w.Write([]byte("pong"))
}

func authorizePatient(w http.ResponseWriter, r *http.Request) {
	token := r.Header.Get("Token")
	if len(token) == 0 {
		mlog.Error(ErrMissing)
		http.Error(w, ErrMissing.Error(), http.StatusForbidden)
		return
	}

	err := auth.CheckKey(token, "patient")
	if err != nil {
		mlog.Error(ErrInvalid)
		http.Error(w, ErrInvalid.Error(), http.StatusUnauthorized)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func authorizeDoctor(w http.ResponseWriter, r *http.Request) {
	token := r.Header.Get("Token")
	if len(token) == 0 {
		mlog.Error(ErrMissing)
		http.Error(w, ErrMissing.Error(), http.StatusForbidden)
		return
	}

	err := auth.CheckKey(token, "doctor")
	if err != nil {
		mlog.Error(ErrInvalid)
		http.Error(w, ErrInvalid.Error(), http.StatusUnauthorized)
		return
	}

	w.WriteHeader(http.StatusOK)
}
