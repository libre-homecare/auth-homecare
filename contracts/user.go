/*

auth-homecare

Copyright (c) 2020, Fco. Javier García Vázquez <xavie2a@gmail.com>

auth-homecare is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
Public License version 3. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
You should have received a copy of the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.

*/

package contracts

// User contains user parameters
type User struct {
	ID       string `json:"id"`
	Username string `json:"username"`
	Password string `json:"password"`
	Role     string `json:"role"`
}

// ChangePass contains chager password parameters
type ChangePass struct {
	ID          string `json:"id"`
	Username    string `json:"username"`
	Password    string `json:"password"`
	NewPassword string `json:"newpassword"`
	Role        string `json:"role"`
}
