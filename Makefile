

#auth-homecare

#Copyright (c) 2020, Fco. Javier García Vázquez <xavie2a@gmail.com>

#auth-homecare is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
#Public License version 3. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
#without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#You should have received a copy of the GNU Affero General Public License along with this program.
#If not, see <http://www.gnu.org/licenses/>.



NAME=auth-homecare
GO=go
VERSION=$(shell cat ./VERSION)
GOFLAGS=-ldflags "-s -w -X main.version=$(VERSION)"

build:
	$(GO) build $(GOFLAGS) -o $(NAME)

arm:
	GOOS=linux GOARCH=arm $(GO) build $(GOFLAGS) -o $(NAME)

compress:
	upx-ucl --brute $(NAME)

clean:
	rm -f $(NAME)

test:
	go test -cover
	go vet
	gofmt -l .

run:
	./$(NAME)
