/*

auth-homecare

Copyright (c) 2020, Fco. Javier García Vázquez <xavie2a@gmail.com>

auth-homecare is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
Public License version 3. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
You should have received a copy of the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.

*/

package db

import (
	"auth-homecare/contracts"
	"errors"

	jsoniter "github.com/json-iterator/go"
	bolt "go.etcd.io/bbolt"
)

// Errors returned by data base.
var (
	ErrNotFound            = errors.New("Item not found")
	ErrUnsupportedDatabase = errors.New("Unsupported database type")
	ErrInvalidObjectID     = errors.New("Invalid object ID")
	ErrNotUnique           = errors.New("Resource already exists")
	ErrObjFound            = errors.New("Item found")
)

const usersBucket = "Users"

// BoltClient is the Bolt client
type BoltClient struct {
	db *bolt.DB // Bolt database
}

// OpenDatabase open a new database connection
// returns, if successful, a pointer to the BoltClient
func OpenDatabase(path string) (*BoltClient, error) {
	var opt bolt.Options

	// Set timeout to wait to known if database is locked (ms)
	opt.Timeout = 5000

	bdb, err := bolt.Open(path, 0600, &opt)
	if err != nil {
		return nil, err
	}

	boltClient := &BoltClient{db: bdb}
	return boltClient, nil
}

// Add an element
func (bc *BoltClient) Add(element interface{}, id string) error {
	return bc.db.Update(func(tx *bolt.Tx) error {
		b, _ := tx.CreateBucketIfNotExists([]byte(usersBucket))
		if b == nil {
			return ErrUnsupportedDatabase
		}
		json := jsoniter.ConfigCompatibleWithStandardLibrary
		encoded, err := json.Marshal(element)
		if err != nil {
			return err
		}
		return b.Put([]byte(id), encoded)
	})
}

// Update an element
func (bc *BoltClient) Update(element interface{}, id string) error {
	return bc.db.Update(func(tx *bolt.Tx) error {
		b, _ := tx.CreateBucketIfNotExists([]byte(usersBucket))
		if b == nil {
			return ErrUnsupportedDatabase
		}
		if b.Get([]byte(id)) == nil {
			return ErrNotFound
		}
		json := jsoniter.ConfigCompatibleWithStandardLibrary
		encoded, err := json.Marshal(element)
		if err != nil {
			return err
		}
		return b.Put([]byte(id), encoded)
	})
}

// GetByID gets an element by ID
func (bc *BoltClient) GetByID(v interface{}, gid string) error {
	err := bc.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(usersBucket))
		if b == nil {
			return ErrNotFound
		}
		encoded := b.Get([]byte(gid))
		if encoded == nil {
			return ErrNotFound
		}
		json := jsoniter.ConfigCompatibleWithStandardLibrary
		err := json.Unmarshal(encoded, &v)
		return err
	})
	return err
}

// GetByNameAndRole gets an element by name
func (bc *BoltClient) GetByNameAndRole(v interface{}, name string, role string) error {
	err := bc.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(usersBucket))
		if b == nil {
			return ErrNotFound
		}
		err := b.ForEach(func(id, encoded []byte) error {
			namevalue := jsoniter.Get(encoded, "username").ToString()
			rolevalue := jsoniter.Get(encoded, "role").ToString()
			if namevalue == name && rolevalue == role {
				json := jsoniter.ConfigCompatibleWithStandardLibrary
				err := json.Unmarshal(encoded, &v)
				if err != nil {
					return err
				}
				return ErrObjFound
			}
			return nil
		})
		if err == nil {
			return ErrNotFound
		} else if err == ErrObjFound {
			return nil
		}
		return err
	})
	return err
}

// GetUsersByRole gets users
func (bc *BoltClient) GetUsersByRole(users *[]contracts.User, role string) error {
	json := jsoniter.ConfigCompatibleWithStandardLibrary
	err := bc.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(usersBucket))
		if b == nil {
			return nil
		}
		*users = []contracts.User{}
		err := b.ForEach(func(id, encoded []byte) error {
			rolevalue := jsoniter.Get(encoded, "role").ToString()
			if rolevalue == role {
				var user contracts.User
				err := json.Unmarshal(encoded, &user)
				if err != nil {
					return err
				}
				*users = append(*users, user)
			}
			return nil
		})
		return err
	})
	return err
}

// DeleteByID deletes from the collection based on ID
func (bc *BoltClient) DeleteByID(id string) error {
	err := bc.db.Update(func(tx *bolt.Tx) error {
		b, _ := tx.CreateBucketIfNotExists([]byte(usersBucket))
		if b == nil {
			return ErrUnsupportedDatabase
		}
		errs := b.Delete([]byte(id))
		if errs != nil {
			return errs
		}
		return nil
	})
	return err
}
