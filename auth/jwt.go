/*

auth-homecare

Copyright (c) 2020, Fco. Javier García Vázquez <xavie2a@gmail.com>

auth-homecare is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
Public License version 3. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
You should have received a copy of the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.

*/

package auth

import (
	"fmt"
	"math/rand"
	"time"

	"github.com/dgrijalva/jwt-go"
)

const (
	charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
)

var secretKey string

// CheckKey checks a JSON Web Token
func CheckKey(key, role string) error {
	if secretKey == "" {
		return fmt.Errorf("Invalid key")
	}

	c := jwt.StandardClaims{}

	token, err := jwt.ParseWithClaims(
		key,
		&c,
		func(val *jwt.Token) (interface{}, error) {
			return []byte(secretKey), nil
		},
	)
	if err != nil {
		return err
	}

	// Validate the token and return the custom claims
	claims, ok := token.Claims.(*jwt.StandardClaims)
	if !ok || !token.Valid {
		return fmt.Errorf("Invalid key")
	}

	if claims.Issuer != role {
		return fmt.Errorf("Invalid role")
	}

	return nil
}

// CreateKey creates a JSON Web Token with a given subject.
func CreateKey(subject, issuer string) (string, error) {
	if secretKey == "" {
		secretKey = generateSecretKey()
	}

	claims := jwt.StandardClaims{
		Issuer:    issuer,
		IssuedAt:  time.Now().UTC().Unix(),
		ExpiresAt: time.Now().UTC().Add(time.Hour * 1).Unix(),
		Subject:   subject,
	}

	key := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	raw, err := key.SignedString([]byte(secretKey))
	if err != nil {
		return "", fmt.Errorf("Error signing key: %v", err)
	}

	return raw, nil
}

func generateSecretKey() string {
	b := make([]byte, 16)
	seededRand := rand.New(rand.NewSource(time.Now().UnixNano()))

	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}
	return string(b)
}

// UsernameFromToken returns User's Name
func UsernameFromToken(key string) string {
	c := jwt.StandardClaims{}
	token, _ := jwt.ParseWithClaims(
		key,
		&c,
		func(val *jwt.Token) (interface{}, error) {
			return []byte(secretKey), nil
		},
	)

	// Validate the token and return the custom claims
	claims, _ := token.Claims.(*jwt.StandardClaims)

	return claims.Subject
}
