/*

auth-homecare

Copyright (c) 2020, Fco. Javier García Vázquez <xavie2a@gmail.com>

auth-homecare is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
Public License version 3. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
You should have received a copy of the GNU Affero General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.

*/

package config

// Configuration contains configuration data
type Configuration struct {
	AuthPort int
	User     string
	Pass     string
	Role     string
}

const (
	authPort    int    = 8080
	defUsername string = "admin"
	defPassword string = "1234"
	defRole     string = "doctor"
)

// LoadConfig returns configuration
func LoadConfig() Configuration {
	cfg := Configuration{
		AuthPort: authPort,
		User:     defUsername,
		Pass:     defPassword,
		Role:     defRole,
	}

	return cfg
}
