module auth-homecare

require (
	github.com/coreos/bbolt v1.3.3 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-zoo/bone v1.3.0
	github.com/jbrodriguez/mlog v0.0.0-20180805173533-cbd5ae8e9c53
	github.com/json-iterator/go v1.1.7
	github.com/satori/go.uuid v1.2.0
	go.etcd.io/bbolt v1.3.3
	golang.org/x/crypto v0.0.0-20191001103751-88343688bb37
)

go 1.13
